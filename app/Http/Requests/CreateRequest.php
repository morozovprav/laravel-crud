<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => "required",
//            'address' => "required",
//            'longitube' => "required",
//            'latitube' => "required",
//            'device_type' => "required",
//            'manufacturer' => "required",
//            'model' => "required",
//            'install_date' => "required",
//            'note' => "required",
//            'eui' => "required",
//            'serial_number' => "required",
        ];
    }
}
