<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Device;


class DeviceController extends Controller
{
    public function create(Request $request){
        $data = new Device();
        $data->name = $request->name;
        $data->save();
        return redirect()->route('record.show');
    }

    public function show(){
        $data = Device::paginate(2);
        return view('admin.main', ['data' => $data]);
    }

    public function delete(Request $request){
        $record = Device::find($request->id);
        $record->delete();
        return redirect()->route('record.show');
    }

    public function update(Request $request){

        $data = Device::where('id', $request->id)->firstOrFail();
        return view('admin.update', ['data' => $data]);

    }

    public function save(Request $request){
        $data = Device::where('id', $request->id)->firstOrFail();
        $data->name = $request->name;
        $data->save();
        return redirect()->route('record.show');
    }


}
