<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRequest;
use App\Models\Device;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;


class DeviceApiController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
      $data = Device::all();

      return $this->sendResponse($data, 'Device show successfully!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRequest $request)
    {

        $data = new Device([
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'longitube' => $request->get('longitube'),
            'latitube' => $request->get('latitube'),
            'device_type' => $request->get('device_type'),
            'manufacturer' => $request->get('manufacturer'),
            'model' => $request->get('model'),
            'install_date' => $request->get('install_date'),
            'note' => $request->get('note'),
            'eui' => $request->get('eui'),
            'serial_number' => $request->get('serial_number'),

        ]);
        $data->save();
        return response()->json([
            'status' => 'seccess'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $data = Device::find($id);
        return response()->json([
            'status' => 'success',
            "data" => $data,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CreateRequest $request, $id)
    {
        $data = Device::find($id);
        $data->name = $request->get('name');
        $data->address = $request->get('address');
        $data->longitube = $request->get('longitube');
        $data->latitube = $request->get('latitube');
        $data->device_type = $request->get('device_type');
        $data->manufacturer = $request->get('manufacturer');
        $data->model = $request->get('model');
        $data->install_date = $request->get('install_date');
        $data->note = $request->get('note');
        $data->eui = $request->get('eui');
        $data->serial_number = $request->get('serial_number');

        $data->save();
        return response()->json([
            'status' => 'seccess'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $data = Device::find($id);
        $data->delete();

        return response()->json([
            'status' => 'success',
        ]);
    }
}
