<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'address',
        'longitube',
        'latitube',
        'device_type',
        'manufacturer',
        'model',
        'install_date',
        'note',
        'eui',
        'serial_number',
    ];

    public function get(){
        $data = Device::query()->get();
        $data = $data->toArray();
        return $data;
    }

}
