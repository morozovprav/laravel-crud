@extends('layouts.app')
@section('content')
@foreach ($data as $record)
    <div>name: {{$record->name}}</div>
    <div>id: {{$record->id}}</div>
    <a href="{{route('record.delete', ['id' => $record->id])}}">Delete</a>
    <a href="{{route('record.update', ['id' => $record->id])}}">Update</a>
    <hr>
@endforeach
{{$data->links()}}
@endsection
