@extends('layouts.app')

@section('content')
<h1>Add form</h1>
<form method="POST" action={{route('record.save', ['id' => $data->id])}} enctype="multipart/form-data">
    @csrf
    <label for="name">Name:</label><br>
    <input type="text" id="name" name="name" value="{{$data->name}}"><br>
    <label for="address">address:</label><br>
    <input type="text" id="address" name="address" value="{{$data->address}}"><br>
    <input type="submit" value="Submit">

  </form>
@endsection
