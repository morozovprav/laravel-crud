<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
</head>
<body>
    <a href="{{route('record.show')}}">Show</a>
    <a href="{{route('record.createform')}}">Create</a>
    <hr>
    @yield('content')
</body>
</html>
