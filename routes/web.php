<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DeviceController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/api-doc', function () {
    return view('api');
});

Route::get('/show', [DeviceController::class, 'show'])
->name('record.show');

Route::view('/create', 'admin.create')->name('record.createform');
Route::post('/create', [DeviceController::class, 'create'])
->name('record.create');

Route::get('/delete', [DeviceController::class, 'delete'])
->name('record.delete');

Route::get('/update', [DeviceController::class, 'update'])
->name('record.update');
Route::post('/save', [DeviceController::class, 'save'])
->name('record.save');


